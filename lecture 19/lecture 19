%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Overleaf (WriteLaTeX) Example: Molecular Chemistry Presentation
%
% Source: http://www.overleaf.com
%
% In these slides we show how Overleaf can be used with standard 
% chemistry packages to easily create professional presentations.
% 
% Feel free to distribute this example, but please keep the referral
% to overleaf.com
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% How to use Overleaf: 
%
% You edit the source code here on the left, and the preview on the
% right shows you the result within a few seconds.
%
% Bookmark this page and share the URL with your co-authors. They can
% edit at the same time!
%
% You can upload figures, bibliographies, custom classes and
% styles using the files menu.
%
% If you're new to LaTeX, the wikibook is a great place to start:
% http://en.wikibooks.org/wiki/LaTeX
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{beamer}

% For more themes, color themes and font themes, see:
% http://deic.uab.es/~iblanes/beamer\_gallery/index\_by\_theme.html
%
\mode<presentation>
{
  \usetheme{Madrid}       % or try default, Darmstadt, Warsaw, ...
  \usecolortheme{default} % or try albatross, beaver, crane, ...
  \usefonttheme{serif}    % or try default, structurebold, ...
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
} 

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{chemfig}
\usepackage[version=3]{mhchem}
\usepackage{tikz}
\usepackage{mathrsfs}
% On Overleaf, these lines give you sharper preview images.
% You might want to `comment them out before you export, though.
\usepackage{pgfpages}
\pgfpagesuselayout{resize to}[%
  physical paper width=8in, physical paper height=6in]

% Here's where the presentation starts, with the info for the title slide
\title{Lecture 19}
\author{Prof. Mark Brown}
\date{April 11, 2016}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%slide 2
\begin{frame}{Brownian motion I}
\underline{Brownian motion I}\\
\vspace{0.3cm}
A standard Brownian motion process, $\{X(t), t\geq 0\}$, is defined by,\\
\vspace{0.2cm}
(i) $X(0)=0$\\
\vspace{0.2cm}
(ii) Stationary increments\\
\vspace{0.2cm}
(iii) independent increments\\
\vspace{0.2cm}
(iv) $X(t)\sim N(0,t)$, for all $t>0$\\
\vspace{0.2cm}
Note that (i), (ii) and (iii) all hold for Poisson processes as well.\\
\vspace{0.3cm}
It can be shown that such a process exists. Moreover with probability 1 its sample paths are continuous but nowhere differentiable functions.\\
\vspace{0.3cm}
If $Y(t)=\sigma X(t)$, with X standard Brownian motion, then Y has property (i)-(iii) with $Y(t)\sim N(0,\sigma^2 t)$. Y is called a Brownian motion process. It is standard Brownian motion if $\sigma=1$.\\





\end{frame}

%%slide 3
\begin{frame}{Brownian motion I}
Note that for $s<t$,
$$Cov(X(s),X(t))=Cov(X(s),X(s)+(X(t)-X(s)))=VarX(s)$$
$$=s\; \text{( where we use independent increments)}$$
Thus,
$$Cov(X(s),X(t))=min(s,t)$$
For $Y=\sigma X,Cov(Y(s),Y(t))=\sigma^2 min(s,t)$. If,

$$\left(\begin{array}{ll}
U \\
V  
\end{array}
\right)
\sim
N\left(
\left(\begin{array}{ll}
EU \\
EV  
\end{array}
\right)
,
\left(\begin{array}{ll}
\sigma_U^2 \; \sigma_{UV} \\
\sigma_{UV} \; \sigma_{V}^2
\end{array}
\right)
\right)
$$
(bivariate normal with mean vector
 $\left(\begin{array}{ll}
U \\
V  
\end{array}
\right)$, and covariance matrix $\left(\begin{array}{ll}
\sigma_U^2 \; \sigma_{UV} \\
\sigma_{UV} \; \sigma_{V}^2
\end{array}
\right)$) , then,








\end{frame}

%%slide 4
\begin{frame}{Brownian motion I}
$$(*) \quad V|U=u \sim N(EV+\frac{\sigma_{UV}}{\sigma_U^2}(u-EU),\sigma_V^2-\frac{\sigma^2_{UV}}{\sigma_U^2})$$
(Standard result for bivariate normal distributed.) Applying this to $V=X(s),U=X(t)$, (where $s<t$), $EU=EV=0, \sigma_V^2=s, \sigma_U^2=t,$\\$ \sigma_{UV}^2=s$ and $u=x$, we obtain,
$$(1)\quad X(s)|X(t)=x \sim N(\frac{s}{t}x,\frac{s(t-s)}{t})$$
For $t_1<t_2<\cdots <t_n$
$$f_{X(t_1),\cdots,X(t_n)}(x_1,\cdots,x_n)=\prod\limits_1^n f_{X(t_i)-X(t_{i-1})}(x_i-x_{i-1})\;\;\; (t_0=0,x_0=0)$$

$$=\prod\limits_1^n[\frac{1}{\sqrt{2\pi (t_i-t_{i-1})}} e^{-\frac{1}{2}\sum\limits_1^n \frac{(x_i-x_{i-1})^2}{t_i-t_{i-1}}}]$$






\end{frame}

%%slide 5
\begin{frame}{Maximum of Brownian motion}
$$(2)\quad =(\frac{1}{2\pi})^{n/2}(\frac{1}{\prod\limits_1^n (\sqrt{t_i -t_{i-1}})})e^{-\frac{1}{2}\sum\limits_1^n \frac{(x_i-x_{i-1})^2}{t_i-t_{i-1}}}$$
Thus (2) gives the joint pdf of $X(t_1),\cdots,X(t_n)$ for $0<t_1<t_2<\cdots$\\$<t_n$, for standard Brownian motion.\\
\vspace{0.3cm}
\underline{Maximum of Brownian motion}\\
\vspace{0.1cm}
\underline{Lemma1} Define, $M(t)=\underset{0\leq s \leq t}{sup} (X(s))$, where $\{X(t), t\geq 0\}$ is standard Brownian motion. We will derive,\\
\vspace{0.1cm}
(i) $Pr(M(t)\leq y, X(t)\leq x)=\Phi(\frac{x}{\sqrt(t)})+\Phi(\frac{2y-x}{\sqrt(t)})-1$. For $x\leq y,y>0.$\\
\vspace{0.1cm}
(ii) $Pr(M(t)\leq y)=2 \Phi(\frac{y}{\sqrt{t}})-1$, for $y>0$. Thus, $M(t)\sim |X(t)|\sim $\\$|N(0,t)|\sim \sqrt{t} |N(0,1)|$\\
\vspace{0.1cm}
(iii) $f_{X(t),M(t)}(x,y)=\sqrt{\frac{2}{\pi}} \frac{1}{t^{3/2}}(2y-x)e^{-\frac{(2y-x)^2}{2t}}$ for $-\infty <x<y, y>0$





\end{frame}

%%slide 6
\begin{frame}{Maximum of Brownian motion}
Before proving the result we'll discuss a first passage time implication.\\
\vspace{0.2cm}
Define $T_a=\{min\;t:X(t)=a\}$. We take $a>0$ but by symmetry $T_{-a}\sim T_a$, so it works for negative a as well. Now,
$$\{T_a>t\} \Leftrightarrow \{M(t)<a\}$$
Thus, $Pr(T_a>t)=2\Phi(\frac{a}{\sqrt{t}})-1\;\;(by \;\;(ii))$
Then,
$$f_{T_a}(t)=-\frac{d}{dt}Pr(T_a>t)=2a(\frac{1}{2t^{3/2}})\phi(\frac{a}{\sqrt{t}})=\frac{a}{\sqrt{2\pi t^{3}}} e^{-\frac{a^2}{2t}}, \; t>0$$
Define,
$$L(t)=e^{\lambda X_t -\frac{\lambda^2 t}{2}}$$
$\{L(t), t\geq 0\}$ is a non-negative, mean 1, martingale since,
$$E[L(t)|\mathcal{F}_s]=E[e^{\lambda X_s}e^{\lambda (X_t -X_s)}e^{-\frac{\lambda^2 t}{2}}|\mathcal{F}_s] \;\;(\mathcal{F}_s=\sigma\{X(u),0\leq u \leq s\})$$


\end{frame}

%%slide 7
\begin{frame}{Maximum of Brownian motion}
$$=e^{\lambda X_s} e^{-\frac{\lambda^2 t}{2}} E[e^{\lambda (X_t -X_s)}|\mathcal{F}_s]=e^{\lambda X_s} e^{-\frac{\lambda^2 t}{2}}E[e^{\lambda (X_t -X_s)}]\;\; $$
$$\text{(by independent increments)}=e^{\lambda X_s} e^{-\frac{\lambda^2 t}{2}}M_{N(0,t-s)}(\lambda) $$
$$=e^{\lambda X_s} e^{-\frac{\lambda^2 t}{2}} e^{\frac{\lambda^2 (t-s)}{2}}=e^{\lambda X_s-\frac{\lambda^2 s}{2}}=L(s)$$
(confirming that L is a martingale). Since $L(0)\equiv 1, EL(t)=1$. Observe that $T_a$ is a stopping time. For $\lambda>0$,
$$|L(t)|=L(t)=e^{\lambda X_t-\frac{\lambda^2 t}{2}} \leq e^{\lambda X_t}\leq e^{\lambda a}  $$

For $t\leq T_a$. This is sufficient condition(i) for the martingale stopping theorem to hold. Thus,
$$1=EL(T_a)=E[e^{\lambda X(T_a)-\frac{\lambda^2 T_a}{2}}]=E[e^{\lambda a-\frac{\lambda^2 T_a}{2}}]=e^{\lambda a} E[e^{-\frac{\lambda^2 T_a}{2}}]$$
$$\Rightarrow 1=e^{\lambda a} E[e^{-\frac{\lambda^2 T_a}{2}}]$$
$$(1)\Rightarrow E[e^{-\frac{\lambda^2 T_a}{2}}]=e^{-\lambda a} $$



\end{frame}

%%slide 8
\begin{frame}{Maximum of Brownian motion}
Let $\alpha=\frac{\lambda^2}{2}$ so that(recall we chose $\lambda>0$), $\lambda=\sqrt{2\alpha}$, and thus (1) is equivalent to,
$$(2)\quad \psi_{T_a}(\alpha)=E(e^{-\alpha T_a})=e^{-\sqrt{2\alpha}a}$$
for all $\alpha \geq 0$; we thus have the Laplace transform of $T_a$. Note that,
$$\psi_{T_a}'(\alpha)=-\sqrt{2} \frac{a}{2\sqrt{\alpha}} e^{-\sqrt{2\alpha}a}=- \frac{a}{\sqrt{2\alpha}}e^{-\sqrt{2\alpha}a}$$
so that, $ET_a= \underset{\alpha \rightarrow 0}{lim}[-\psi_{T_a}'(\alpha)]=\infty$, and that $T_a$ which is finite with probability 1, has infinite mean. We also can show that,
$$(3)\quad T_a \sim \frac{a^2}{Z^2}, \;with \; Z\sim N(0,1)$$
\underline{proof of (3)}\\
$$Pr(\frac{a^2}{Z^2}>t)=Pr(Z^2 <\frac{a^2}{t})=Pr(|Z| < \frac{a}{\sqrt{t}})=2\Phi(\frac{a}{\sqrt{t}})-1$$
$$=Pr(M(t)<a) \;(see\;(ii))=Pr(T_a>t)$$

\end{frame}

%%slide 9
\begin{frame}{Observation}

From (3), $ET_a^{-r}=\frac{1}{a^{2r}}EZ^{2r}=\frac{1(3)(5)\cdots(2r-1)}{a^{2r}}$\\
\vspace{0.3cm}
\underline{Observation} Since for $a>0,b>0,$
$$T_{a+b}\sim T_a+T_b,$$
$\psi_{_{T_{a+b}}}(\alpha)=\psi_{_{T_{a}}}(\alpha)\psi_{_{T_{b}}}(\alpha)$. For fixed $\alpha$, the only solution to this equation which lies in (0,1) is,
$$\psi_{_{T_a}}(\alpha)=e^{-C_\alpha a}$$
where $C_\alpha>0$; (3) shows that $C_\alpha=\sqrt{2\alpha}$\\
Now we finally prove Lemma1. We first note that for any $s \geq 0,\;$\\$ \{X(s+u)-X(s),u\geq 0\}$ is a standard Brownian motion process. It satisfies (i)-(iv) of the definition on page 1. If we replace s by a stopping time Y it is plausible, and can be proved that,
$$\{X(Y+u)-X(Y), u\geq 0\}$$
is a standard Brownian motion process as well. Setting $Y=T_a,$









\end{frame}

%%slide 10
\begin{frame}{Observation}
$$\{X(Y+u)-X(Y)\}=\{X(T_a+u)-X(T_a)\}=\{X(T_a+u)-a,u\geq 0\}$$
is a standard Brownian motion process. Thus for all $u>0, b>0$
$$Pr(X(T_a+u)-a\geq b)=Pr(X(T_a+u)-a \leq -b)$$
equivalently,
$$(4) \quad Pr(X(T_a+u)\geq a+b)=Pr(X(T_a+u)\leq a-b)$$
Consequently, for $y>0, x\leq y$, (from (4))
$$Pr(X(t)\leq x|M(t)\geq y)=Pr(X(t)\leq y-(y-x)|M(t)\geq y)=$$
$$Pr(X(t)\geq y+(y-x)|M(t)\geq y)=Pr(X(t)\geq 2y-x|M(t)\geq y)$$
Thus,
$$Pr(X(t)\leq x, M(t)\geq y)=Pr(M(t)\geq y)Pr(X(t)\leq x|M(t)\geq y)=$$
$$Pr(M(t)\geq y)Pr(X(t)\geq 2y-x|M(t) \geq y)$$


\end{frame}

%%slide 11
\begin{frame}{Observation}
$$(5)=Pr(X(t)\geq 2y-x, M(t)\geq y)=Pr(X(t)\geq 2y-x)=1-\Phi(\frac{2y-x}{\sqrt{t}})$$
since, $\{X(t) \geq y+(y-x)\} \Rightarrow \{M(t)\geq X(t)\geq y\}$. 
Next, $\Phi(\frac{x}{\sqrt{t}})=$\\$Pr(X(t)\leq x)=Pr(X(t)\leq x, M(t)\leq y)+Pr( X(t)\leq x, M(t)\geq y)=Pr(X(t)\leq x, M(t)\leq y)+1-\Phi(\frac{2y-x}{\sqrt{t}}) \;\;\;by\;(5)$
Thus, $$F_{X(t),M(t)}(x,y)=\Phi(\frac{x}{\sqrt{t}})+\Phi(\frac{2y-x}{\sqrt{t}})-1$$
which is (i). For (ii)
$$Pr(M(t)\leq y)=F_{X(t),M(t)}(y,y)=2\Phi(\frac{y}{\sqrt{t}})-1=Pr(|N(0,t)|\leq y)$$
Thus, $M(t)\sim |N(0,t)| \sim \sqrt{t} N(0,1) $. For (iii)
$$f_{X(t),M(t)}(x,y)=\frac{\partial^2}{\partial x \partial y}F_{X(t),M(t)}(x,y)$$

\end{frame}

%%slide 12
\begin{frame}{Observation}

and,
$$\frac{\partial}{ \partial y}F_{X(t),M(t)}(x,y) =\frac{2}{\sqrt{t}} \phi(\frac{2y-x}{\sqrt{t}})$$
Now,
$$\phi'(s)=\frac{d}{ds}(ce^{-s^2/2})=-cse^{-s^2/2}=-s\phi(s)$$
Thus,
$$\frac{\partial}{ \partial x}(\frac{\partial}{ \partial y}F_{X(t),M(t)}(x,y))=\frac{2}{\sqrt{t}}[\frac{\partial}{ \partial x} \phi(\frac{2y-x}{\sqrt{t}})]$$

$$=\frac{2}{\sqrt{t}}(-\frac{1}{\sqrt{t}})(-\frac{2y-x}{\sqrt{t}}\phi(\frac{2y-x}{\sqrt{t}}))=\frac{2(2y-x)}{t^{3/2}}\frac{1}{\sqrt{2\pi}} e^{-\frac{(2y-x)^2}{2t}}$$
$$=\sqrt{\frac{2}{\pi t^3}}(2y-x)e^{-\frac{(2y-x)^2}{2t}}$$
which is (iii)





\end{frame}

%%slide 13
\begin{frame}{Transformations preserving Brownian motion}
\underline{Transformations preserving Brownian motion}\\
\vspace{0.2cm}
A normal or Gaussian process is determined by its mean function, $\mu (t)=EX(t)$, its covariance kernel, $K(s,t)=Cov(X(s),X(t))$, and the requirement that for any n and $t_1<t_2\cdots<t_n$, that $(X(t_1),\cdots,$\\$X(t_n))$, be multivariate normally distributed.\\
\vspace{0.2cm}
To show that a Gaussian process Y with $Y(0)\equiv 0$ is standard Brownian motion it is sufficient to show that its mean function is zero, and that its covariance kernel is $K(s,t)=min(s,t)$.\\
\vspace{0.1cm}
Let $\{X(t), t\geq 0\}$ be standard Brownian motion. Then the following transformed versions are also standard Brownian motion:\\
\vspace{0.1cm}
(i) $\{-X(s), s\geq 0\}$\\
\vspace{0.1cm}
(ii) $\{Y(s)=X(t+s)-X(t), 0\leq s< \infty\}$.\\
\vspace{0.1cm}
(iii) $\{Z(s)=sX(\frac{1}{s}),s>0,\; with \; Z(0)\equiv 0\}$\\
\vspace{0.1cm}
(iv) $\{W(s)=X(t)-X(t-s), 0\leq s\leq t\}$(Brownian motion process on [0,t])









\end{frame}

%%slide 14
\begin{frame}{Arc-sine Law}
For example to prove (iii):
$$EZ(s)=sEX(\frac{1}{s})=s(0)=0,\; for \; s\geq 0.$$
For $s\leq t$, $$Cov(Z(s),Z(t))=stCov(X(\frac{1}{s}),X(\frac{1}{t}))=st\;min(\frac{1}{s},\frac{1}{t})=st(\frac{1}{t})=s$$
\underline{Arc-sine Law}\\
Let $\{X(t), t\geq 0\}$ be standard Brownian motion. Then,
$$Pr(X(t) \text{ has no zeroes in }(t_1,t_2))=\frac{2}{\pi}sin^{-1}(\sqrt{\frac{t_1}{t_2}})$$
for, $0<t_1<t_2$. This will be proved below.\\
\underline{Consequences}\\
(1) Define, $L_t=max\{s<t: X(s)=0\}$, the last zero in [0,t]. It follows from the arc-sine law that,




\end{frame}

%%slide 15
\begin{frame}{Arc-sine Law}
$$Pr(L_t<s)=Pr(\text{no zeroes in [s,t]})=\frac{2}{\pi}sin^{-1}(\sqrt{\frac{s}{t}})$$
Consequently,
$$Pr(L_t=0)=\underset{s\rightarrow 0}{lim} Pr(L_t <s)=0$$
Thus for any $\epsilon>0$, no matter how small the Brownian motion process is guaranteed to have a zero in $(0,\epsilon)$. It follows that the process has infinitely many zeroes in $(0,\epsilon)$, with probability 1, for all $\epsilon >0$.\\
(2) $$f_{L_t}(s)=\frac{d}{ds}Pr(L_t <s)=\frac{2}{\pi} \frac{1}{\sqrt{1-(\sqrt{\frac{s}{t}})^2}} \frac{1}{\sqrt{t}} \frac{1}{2\sqrt{s}}$$
$$=\frac{1}{\pi}\frac{1}{\sqrt{s(t-s)}}, 0<s<t$$
The pdf is minimized at $s=\frac{t}{2}$, and approaches $\infty$ as $s\rightarrow 0$ and as $s\rightarrow t$. It is symmetric about t/2.\\






\end{frame}

%%slide 16
\begin{frame}{Arc-sine Law}
(3) Consider, $Pr(X(s)>0,X(t)>0), s<t$.\\ Now,
 $$Pr(X(t)>0|X(s)>0)=$$
 $$Pr(\text{no zero in [s,t]}|X(s)>0)+Pr(\text{at least one zero in [s,t]}|X(s)>0)\times$$
$$Pr(X(t)>0|X(s)>0, \text{ at least one zero in [s,t]})=$$
$$\frac{2}{\pi}sin^{-1}(\sqrt{\frac{s}{t}})+\frac{1}{2}[1-\frac{2}{\pi}sin^{-1}(\sqrt{\frac{s}{t}})]=\frac{1}{2}[1+\frac{2}{\pi}sin^{-1}(\sqrt{\frac{s}{t}})]$$
Thus,
$$Pr(X(s)>0,X(t)>0)=\frac{1}{2}Pr(X(t)>0|X(s)>0)$$
$$=\frac{1}{4}[1+\frac{2}{\pi}sin^{-1}(\sqrt{\frac{s}{t}})]=Pr(X(s)<0,X(t)<0)$$








\end{frame}

%%slide 17
\begin{frame}{Derivation of Arc-Sine Law}
 
\underline{Example} $s=\frac{t}{2}$. Then,
$$Pr(X(\frac{t}{2})>0, X(t)>0)=Pr(X(\frac{t}{2})<0, X(t)<0)=$$
$$\frac{1}{4}[1+\frac{2}{\pi}sin^{-1}(\sqrt{\frac{1}{2}})]=\frac{1}{4}[1+\frac{2}{\pi}\frac{\pi}{4}]=\frac{3}{8}$$
and,
$$Pr(X(\frac{t}{2})>0, X(t)<0)=Pr(X(\frac{t}{2})<0, X(t)>0)=\frac{1}{8}$$
\underline{Derivation of Arc-Sine Law}\\
$$Pr(\text{at least one zero in }(t_1,t_2))$$
$$(A)=2\int\limits_0^\infty f_{X(t_1)}(x)Pr(T_x \leq t_2-t_1)dx=$$





\end{frame}

%%slide 18
\begin{frame}{Derivation of Arc-Sine Law}
$$2\int\limits_{x=0}^\infty \frac{1}{\sqrt{t_1}} \phi(\frac{x}{\sqrt{t_1}})[\int_{y=0}^{t_2-t_1}\frac{x}{y^{3/2}} \phi(\frac{x}{\sqrt{y}})dy]dx=$$
$$2\int\limits_{y=0}^{t_2-t_1} \frac{1}{\sqrt{t_1}}\frac{1}{y^{3/2}}  [\int_{x=0}^{\infty}\frac{1}{2\pi} e^{-\frac{x^2}{2t_1}} e^{-\frac{x^2}{2y}}  dx]dy=2\int\limits_{y=0}^{t_2-t_1} \frac{1}{\sqrt{t_1}}\frac{1}{y^{3/2}} \frac{1}{2\pi}  \frac{t_1 y}{t_1+y}dy=$$
$$\frac{\sqrt{t_1}}{\pi} \int\limits_{y=0}^{t_2-t_1} \frac{1}{y^{1/2}(y+t_1)}dy$$
Substitute, $y=t_1 tan^2\theta,$ then $y^{1/2}=\sqrt{t_1}tan\theta$, and $y+t_1=t_1 sec^2 \theta$, and $dy=2t_1tan\theta sec^2\theta d\theta,$ thus,
$$\frac{\sqrt{t_1}}{\pi} \int\limits_{y=0}^{t_2-t_1} \frac{dy}{y^{1/2}(y+t_1)}=\frac{2\sqrt{t_1}}{\pi} \int\limits_{\theta=0}^{tan^{-1}\sqrt{\frac{t_2-t_1}{t_1}}} \frac{t_1 tan\theta sec^2 \theta}{t_1^{3/2}tan\theta sec^2 \theta}d\theta=$$







\end{frame}

%%slide 19
\begin{frame}{Derivation of Arc-Sine Law}
$$\frac{2}{\pi} \int\limits_{0}^{tan^{-1}\sqrt{\frac{t_2-t_1}{t_1}}} d\theta=\frac{2}{\pi} tan^{-1}\sqrt{\frac{t_2-t_1}{t_1}}$$
Since, $$sin(\frac{\pi}{2}-\theta)=\sqrt{\frac{t_1}{t_2}},\; \theta=\frac{\pi}{2}-sin^{-1}(\sqrt{\frac{t_1}{t_2}})$$ Thus,
$$Pr(\text{at least one zero in }(t_1,t_2))=\frac{2}{\pi}[\frac{\pi}{2}-sin^{-1}(\sqrt{\frac{t_1}{t_2}})]=1-\frac{2}{\pi}sin^{-1}(\sqrt{\frac{t_1}{t_2}})$$
and,
$$Pr(\text{no zeroes in }(t_1,t_2))=\frac{2}{\pi}sin^{-1}(\sqrt{\frac{t_1}{t_2}})$$
which is the arc-sin law.





\end{frame}

\end{document}





 